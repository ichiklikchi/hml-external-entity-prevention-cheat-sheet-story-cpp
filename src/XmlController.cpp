#include "XmlController.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <cstring>

namespace xml_inj {
    
    namespace {

        int strcmp(const char *s1, const char *s2) {
	        if (s1 == nullptr) s1 = "";
	        if (s2 == nullptr) s2 = "";
	        return strcmp(s1, s2);
        }   

        bool streq(const char *s1, const char *s2) {
	        return (::strcmp(s1, s2) == 0);
        }

        void getCreds(xmlNodePtr root, Creds* cread) {
            while (root) {
                if (streq((const char*) (root->name), "user") && root->children) {
                    cread->user = (const char*)(root->children->content);
                    root = root->next;
                } else if (streq((const char*) (root->name), "pass") && root->children) {
                    cread->pass = (const char*)(root->children->content);
                    root = nullptr;
                } else {
                    root = root->children;
                }
            }
        }
    }

    XMLController::XMLController() {
        LIBXML_TEST_VERSION;
    }

    std::string XMLController::create(std::string_view user, std::string_view pass) {
        xmlInitParser();
        /* 
         * Creates a new document, a node and set it as a root node
         */
        xmlParserCtxtPtr ctxt = xmlNewParserCtxt();

        xmlDocPtr doc = xmlCtxtReadFile(ctxt, "./resources/request.xml", nullptr, NULL);
        xmlNodePtr root_node = xmlNewNode(nullptr, BAD_CAST "creds");
        xmlDocSetRootElement(doc, root_node);

        /* 
         * xmlNewChild() creates a new node, which is "attached" as child node
         * of root_node node. 
         */
        xmlNewChild(root_node, nullptr, BAD_CAST "user", BAD_CAST user.data());
        if (not pass.empty()) {
            xmlNewChild(root_node, nullptr, BAD_CAST "pass",
                        BAD_CAST pass.data());
        }

        xmlChar* resultXml;
        int size;
        xmlDocDumpMemory(doc, &resultXml, &size);


        std::string result(reinterpret_cast<char*>(resultXml));

        xmlStopParser(ctxt);
        
        /*free the document */
        xmlFreeDoc(doc);
        xmlCleanupParser();

        return result;
    }

    Creds XMLController::parse(std::string_view xml) {
        xmlInitParser();
        
        xmlParserCtxtPtr ctxt = xmlNewParserCtxt();

        xmlDocPtr doc = xmlCtxtReadMemory(ctxt, xml.data(), xml.length(), nullptr, nullptr, NULL);
        xmlNodePtr root_node = xmlDocGetRootElement(doc);

        Creds cread;
        getCreds(root_node, &cread);
        
        xmlStopParser(ctxt);

        xmlFreeDoc(doc);
        /*
         *Free the global variables that may
         *have been allocated by the parser.
         */
        xmlCleanupParser();

        return cread;

    }

} // namespace xml_inj