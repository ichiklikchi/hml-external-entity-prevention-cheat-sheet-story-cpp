#pragma once
#include <string>
#include <memory>

namespace xml_inj {

    class IStorage {
        public:
            virtual ~IStorage() = default;
            /*
             * \brief chech user login and password
             */  
            virtual bool hasUser(
                std::string_view login, 
                std::string_view password = {}) const = 0;
    };

    using StoragePtr = std::unique_ptr<IStorage>;
    StoragePtr createStorage();

} // namespace xml_inj
