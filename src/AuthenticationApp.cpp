#include <iostream>
#include "AuthenticationApp.h"

namespace xml_inj {
    
    AuthenticationApp::AuthenticationApp() 
        : m_storage(createStorage()) {
    }

    void AuthenticationApp::run() {
        std::string login, password;

        std::cout << "Enter login: ";
        std::cin >> login;

        if (not existsLogin(login)) {
            std::cout << "No login is found, " << login; 
        } else {

            std::cout << "Ok, " << login << ", enter your password: ";
            std::cin >> password;
            
            std::cout << "Password is" << (existsPassword(login, password)? "": "n't") << " correct!";
        }
        std::cout << "\nGoodbye!" << std::endl;
    }
    
    bool AuthenticationApp::existsLogin(std::string_view login) {
        return (m_storage->hasUser(login));
    }
 
    bool AuthenticationApp::existsPassword(std::string_view login, std::string_view password) {
        return (m_storage->hasUser(login, password));
    }

} // namespace xml_inj
