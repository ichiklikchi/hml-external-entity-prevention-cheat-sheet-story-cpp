#pragma once
#include "IStorage.h"

namespace xml_inj {

    class AuthenticationApp {
        public:
            AuthenticationApp();
            /*
             * \brief Run application 
             */
            void run();
        private:
            /*
             * \brief checks if login exists and take him from console
             */
            bool existsLogin(std::string_view login);
            /*
             * \brief checks if password exists and take him from console
             */
            bool existsPassword(std::string_view login, std::string_view password);
        private:
            StoragePtr m_storage;
    };

} // namespace xml_inj
