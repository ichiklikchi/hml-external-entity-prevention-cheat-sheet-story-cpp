#include "AuthenticationApp.h"

int main() {
    xml_inj::AuthenticationApp app;
    app.run();
    
    return 0;
}
