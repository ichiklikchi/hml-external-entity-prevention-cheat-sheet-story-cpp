#pragma once

#include <string_view>
#include <string>

namespace xml_inj {
    struct Creds {
        std::string pass;
        std::string user;
    };

    class XMLController {
        public:
            XMLController();
        
            std::string create(std::string_view user, std::string_view pass);
            Creds parse(std::string_view xml);
    };

} // namespace xml_inj