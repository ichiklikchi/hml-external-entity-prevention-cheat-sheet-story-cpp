#pragma once
#include "IStorage.h"
#include <map>

namespace xml_inj {

    class LocalStorage : public IStorage {
            using Users = std::map<std::string, std::string, std::less<>>;
        public:
            LocalStorage();
            bool hasUser(
                std::string_view login, 
                std::string_view password) const override;

        private:
            Users m_users;
    };
} // namespace xml_inj
